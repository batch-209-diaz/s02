// util.js

function getCircleArea(radius){

	return 3.1416*(radius**2);

}

function checkIfPassed(score,total) {

	return (score/total)*100 >= 75;

}

function getAverage(num1,num2,num3,num4){

	return (num1 + num2 + num3 + num4)/4

}

function getSum(num1,num2){

	return num1 + num2

}

function getDifference(num1,num2){

	return num1 - num2

}

/* S2 ACTIVITY */

function div_check(num){

	if (num % 5 == 0 || num % 7 == 0){
		return true
	} else {
		return false
	}

}

function isOddOrEven(num){

	if (num % 2 == 0){
		return "even"
	} else {
		return "odd"
	}

}

// Stretch Goal

function reverse(string){

	return string.split("").reverse().join("");

}



module.exports = {
	getCircleArea: getCircleArea,
	checkIfPassed: checkIfPassed,
	getAverage: getAverage,
	getSum: getSum,
	getDifference: getDifference,
	div_check: div_check,
	isOddOrEven: isOddOrEven,
	reverse: reverse
}