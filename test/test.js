// test.js

const { assert } = require('chai');
const { getCircleArea } = require('../src/util.js');
const { checkIfPassed } = require('../src/util.js');
const { getAverage } = require('../src/util.js');
const { getSum } = require('../src/util.js');
const { getDifference } = require('../src/util.js');
const { div_check } = require('../src/util.js');
const { isOddOrEven } = require('../src/util.js');
const { reverse } = require('../src/util.js');



describe('test_get_Average', () => {

	it('test_area_of_circle_radius_15_is_706.86', () => {

		let area_1 = getCircleArea(15);
		assert.equal(area_1,706.86);

	});

	it('test_area_of_circle_radius_300_is_282744', () => {

		let area_2 = getCircleArea(300);
		assert.equal(area_2,282744);

	});

});


describe('test_check_if_passed', () => {

	it('test_25_out_of_30_is_passed', () => {

		let isPassed = checkIfPassed(25,30);
		assert.equal(isPassed,true);

	});

	it('test_30_out_of_50_is_not_passed', () => {

		let isPassed = checkIfPassed(30,50);
		assert.equal(isPassed,false);

	});

});


describe('test_get_average', () => {

	it('average_of_80_82_84_86_is_83', () => {

		let average_1 = getAverage(80,82,84,86);
		assert.equal(average_1,83);

	});

	it('average_of_70_80_82_84_is_79', () => {

		let average_2 = getAverage(70,80,82,84);
		assert.equal(average_2,79);

	});

});

describe('test_check_sum', () => {

	it('sum_of_15_30_is_45', () => {

		let sum_1 = getSum(15,30);
		assert.equal(sum_1,45);

	});

	it('sum_of_25_50_is_75', () => {

		let sum_2 = getSum(25,50);
		assert.equal(sum_2,75);

	});

});

describe('test_check_difference', () => {

	it('difference_of_70_40_is_30', () => {

		let diff_1 = getDifference(70,40);
		assert.equal(diff_1,30);

	});

	it('difference_of_125_50_is_75', () => {

		let diff_2 = getDifference(125,50);
		assert.equal(diff_2,75);

	});

});

/* S2 ACTIVITY */

describe('test_divisibility_check', () => {

	//pass
	it('is_55_divisible_by_5', () => {

		let div_1 = div_check(55);
		assert.equal(div_1,true);

	});

	//pass
	it('is_325_divisible_by_5', () => {

		let div_2 = div_check(325);
		assert.equal(div_2,true);

	});

	//pass
	it('is_21_divisible_by_7', () => {

		let div_3 = div_check(21);
		assert.equal(div_3,true);

	});

	//fail
	it('is_143_divisible_by_7', () => {

		let div_4 = div_check(143);
		assert.equal(div_4,true);

	});

});


describe('test_odd_or_even', () => {

	//pass
	it('is_198_even', () => {

		let num1 = isOddOrEven(198);
		assert.equal(num1,'even');

	});

	//fail
	it('is_209_even', () => {

		let num2 = isOddOrEven(209);
		assert.equal(num2,'even');

	});

	//pass
	it('is_921_odd', () => {

		let num3 = isOddOrEven(921);
		assert.equal(num3,'odd');

	});

	//fail
	it('is_296_odd', () => {

		let num4 = isOddOrEven(296);
		assert.equal(num4,'odd');

	});

});

//Stretch Goal

describe('test_reversed_string', () =>{

	it('is_wasabi_string_reversed', () => {

		let revString = reverse('wasabi');
		assert.equal(revString,'ibasaw');

	})

	it('is_steam_string_reversed', () => {

		let revString = reverse('steam');
		assert.equal(revString,'maets');

	})

	function reverseString(str) {
		var newString = "";
		for (var i = str.length - 1; i >= 0; i--) {
			newString += str[i];
		}
		return newString;
	}

	it('is_keyboard_string_reversed', () => {

		let revString = reverse('keyboard');
		let testrevString = reverseString('keyboard');
		assert.equal(revString,testrevString);

	})

	it('is_ice_cream_string_reversed', () => {

		let revString = reverse('ice cream');
		let testrevString = reverseString('ice cream');
		assert.equal(revString,testrevString);

	})


})